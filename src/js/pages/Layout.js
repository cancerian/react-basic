import React from "react";
import { Link } from "react-router";

import Footer from "../components/layout/Footer"
import Nav from "../components/layout/Nav"

export default class Layout extends React.Component {
	navigate() {
		//Push state preserves history and enables back button, now deprecated
		// replaceState disables back button, also deprecated
		this.props.history.replaceState(null, "/");
	}

	render() {
		const { location } = this.props;
		const { history } = this.props;
		const containerStyle = {
			marginTop: "60px"
		};
		console.log(history.isActive("archives"));
		return (
			<div>
				<Nav location={location} />
				<div class="container" style={containerStyle}>
					<div class="row">
						<div class="col-lg-12">
							<h1>KillerNews.net</h1>

							{this.props.children}

						</div>
					</div>
					<Footer/>
				</div>
			</div>
		);
	}
}
